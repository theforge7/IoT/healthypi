from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient


class AwsIotOrchestrator:
    def __init__(self, thing_name, host, port, root_ca_path, private_key_path, certificate_path, request_delay):
        self.thing_name = thing_name
        self.host = host
        self.port = port
        self.root_ca_path = root_ca_path
        self.private_key_path = private_key_path
        self.certificate_path = certificate_path
        self.request_delay = request_delay

    # Configures the MQTT client for this thing.
    def configure_mqtt_client(self):
        mqtt_client = AWSIoTMQTTClient(self.thing_name)
        mqtt_client.configureEndpoint(self.host, self.port)
        mqtt_client.configureCredentials(self.root_ca_path, self.private_key_path, self.certificate_path)
        mqtt_client.configureAutoReconnectBackoffTime(1, 32, 20)
        mqtt_client.configureConnectDisconnectTimeout(10)
        mqtt_client.configureMQTTOperationTimeout(5)

        # AWSIoTMQTTClient connection configuration
        mqtt_client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        mqtt_client.configureDrainingFrequency(2)  # Draining: 2 Hz

        return mqtt_client
