import serial

from src.healthypi4.healthy_pi_data import HealthyPiData

'''
Documentation: https://healthypi.protocentral.com
'''


class HealthyPi:
    def __init__(self):
        self.pi_port = '/dev/ttyAMA0'
        self.baud_rate = 115200
        self.timeout = 1

        self.ser = serial.Serial(
            port=self.pi_port,
            baudrate=self.baud_rate,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=self.timeout)

    def process_line(self):
        line = self.ser.readline()
        return HealthyPiData(line.hex())
