from src import util

'''
Format from http://healthypi3.protocentral.com/#streaming-packet-format
'''


class HealthyPiData:
    def __init__(self, line):
        self.ecg = line[8:12]
        self.resp = line[12:16]
        self.spo2_IR = line[16:24]
        self.spo2_RED = line[24:32]
        self.temp = line[32:36]
        self.global_resp_rate = line[36:38]
        self.global_spo2 = line[38:40]
        self.global_heart_rate = line[40:42]
        self.bp_sys = line[42:44]
        self.bp_dia = line[44:46]
        self.lead_status = line[46:48]

        self.f_ecg = util.__to_little_endian(self.ecg) / 10 ** 3

        self.f_resp = util.__to_little_endian(self.resp) / 10 ** 3

        self.f_spo2_IR = util.__to_little_endian(self.spo2_IR)
        self.f_spo2_RED = util.__to_little_endian(self.spo2_RED)

        self.f_temp = util.__to_little_endian(self.temp) / 100

        self.f_global_resp_rate = util.__to_little_endian(self.global_resp_rate)
        self.f_global_spo2 = util.__to_little_endian(self.global_spo2)
        self.f_global_heart_rate = util.__to_little_endian(self.global_heart_rate)
        self.f_bp_sys = util.__to_little_endian(self.bp_sys)
        self.f_bp_dia = util.__to_little_endian(self.bp_dia)
        self.raw = line

    def __repr__(self):
        return str(self.__to_map__())

    def __to_map__(self):
        return {
            'ecg': self.f_ecg,
            'resp': self.f_resp,
            'temp': self.f_temp,
            'spo2': self.f_spo2_IR,
            'global_resp_rate': self.f_global_resp_rate,
            'global_spo2': self.f_global_spo2,
            'global_heart_rate': self.f_global_heart_rate,
            'bp_sys': self.f_bp_sys,
            'bp_dia': self.f_bp_dia,
            'raw': self.raw
        }
