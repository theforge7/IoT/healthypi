def __to_little_endian(data, signed=True):
    t_data = bytearray.fromhex(data)
    return int.from_bytes(t_data, 'little', signed=signed)
