import argparse
import json
import logging
import time

from src.aws.aws_iot_orchestrator import AwsIotOrchestrator
from src.healthypi4.healthy_pi import HealthyPi


# Configures the argument parser for this program.
def configure_parser():
    configured_parser = argparse.ArgumentParser()
    configured_parser.add_argument('-e', '--endpoint', action='store', required=True, dest='host',
                                   help='Your AWS IoT custom endpoint')
    configured_parser.add_argument('-r', '--rootCA', action='store', required=True, dest='root_ca_path',
                                   help='Root CA file path')
    configured_parser.add_argument('-c', '--cert', action='store', required=True, dest='certificate_path',
                                   help='Certificate file path')
    configured_parser.add_argument('-k', '--key', action='store', required=True, dest='private_key_path',
                                   help='Private key file path')
    configured_parser.add_argument('-p', '--port', action='store', dest='port', type=int, default=8883,
                                   help='Port number override')
    configured_parser.add_argument('-n', '--thing_name', action='store', required=True, dest='thing_name',
                                   help='Targeted thing name')
    configured_parser.add_argument('-d', '--request_delay', action='store', dest='request_delay', type=float, default=1,
                                   help='Time between requests (in seconds)')
    configured_parser.add_argument('-v', '--enableLogging', action='store_true', dest='enable_logging',
                                   help='Enable logging for the AWS IoT Device SDK for Python')
    configured_parser.add_argument('-t', '--topic', action='store', dest='topic', default='topic_1',
                                   help='Targeted topic')
    return configured_parser


# Configures debug logging for the AWS IoT Device SDK for Python.
def configure_logging():
    logger = logging.getLogger('AWSIoTPythonSDK.core')
    logger.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)


# Runs the client with user arguments.
if __name__ == '__main__':
    parser = configure_parser()
    args = parser.parse_args()
    if args.enableLogging:
        configure_logging()
    thing_client = AwsIotOrchestrator(
        args.thing_name,
        args.host,
        args.port,
        args.root_ca_path,
        args.private_key_path,
        args.certificate_path,
        args.request_delay)

    print('Connecting MQTT client for {}...'.format(thing_client.thing_name))
    mqtt_client = thing_client.configure_mqtt_client()
    mqtt_client.connect()
    print('MQTT client for {} connected'.format(thing_client.thing_name))

    print('Running sensor data send for {}...\n'.format(thing_client.thing_name))

    healthy_pi = HealthyPi()

    while True:
        sensor_data = healthy_pi.process_line().__to_map__()
        print('Sending data to [{}]'.format(thing_client.thing_name), sensor_data)
        mqtt_client.publish(args.topic, json.dumps(sensor_data), 1)
        time.sleep(thing_client.request_delay)
